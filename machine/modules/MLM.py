from tqdm import tqdm# Libreria para generar visualizacion de progreso de procesos

import logging # libreria para hacer uso de las clases de loggeo y manejo de registros de alarmas de python
import os # Libreria para utilizar estandares del sistema donde se corre el codigo
import sys # Libreria para manejar llamados al sisyema donde se corre el codigo

stderr = sys.stderr
sys.stderr = open(os.devnull, 'w')
import keras # Libreria para utilizar la intefaz para utilizar tensorflow
from keras.models import Sequential # importacion de modulos prefabs de keras 
from keras.layers import Dense, Dropout # Libreria para importar modelos de capas pefabs de keras
from keras.models import load_model # Libreria para importar modelo con funciones para el manejo de modelos entrenados por keras
from keras.optimizers import Adadelta, Adam
sys.stderr = stderr

class MachineLearningModels():
    "En esta clase se encuentran los modelos de regresion linial para el uso del entrenamiento de modulos predictivos para sistemas de aprendizaje supervisado"
    
    logger= logging
    

    def __init__(self,logger):
        self.logger = logger
        self.ada=Adadelta()
        self.adam = Adam()
        
        
    
    def linear_regression(self, n_inputs, config ):
        
        self.logger.info("Se ha elegido un modelo de regrecion lineal.")

        model = Sequential()

        model.add(Dense(config.get('hide_ly')[0], activation =config.get('fun_activ'), input_shape=(n_inputs,)))
        
        if len(config.get('hide_ly'))>1:
            for nodes in config.get('hide_ly')[1:]:
                model.add(Dense(nodes, activation =config.get('fun_activ')))
                #model.add(Dropout(config.get('dropout')))
        
        model.add(Dense(config.get('n_output')))
        model.compile(optimizer=self.adam, loss='mean_squared_error')
            
        return model
    
    
    