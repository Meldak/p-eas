#!/bin/bash

cat showers1k_h_P.csv | awk -F"," '{if (($1>=16.625)&&($1<=16.875)) print; }'| sort -t ',' -n -k 1 > Pbins/showers1k_h_P_16-625_16-875.csv
cat showers1k_h_P.csv | awk -F"," '{if (($1>=16.875)&&($1<=17.125)) print; }'| sort -t ',' -n -k 1 > Pbins/showers1k_h_P_16-875_17-125.csv
cat showers1k_h_P.csv | awk -F"," '{if (($1>=17.125)&&($1<=17.375)) print; }'| sort -t ',' -n -k 1 > Pbins/showers1k_h_P_17-125_17-375.csv
cat showers1k_h_P.csv | awk -F"," '{if (($1>=17.375)&&($1<=17.625)) print; }'| sort -t ',' -n -k 1 > Pbins/showers1k_h_P_17-375_17-625.csv
cat showers1k_h_P.csv | awk -F"," '{if (($1>=17.625)&&($1<=17.875)) print; }'| sort -t ',' -n -k 1 > Pbins/showers1k_h_P_17-625_17-875.csv
cat showers1k_h_P.csv | awk -F"," '{if (($1>=17.875)&&($1<=18.125)) print; }'| sort -t ',' -n -k 1 > Pbins/showers1k_h_P_17-875_18-125.csv
cat showers1k_h_P.csv | awk -F"," '{if (($1>=18.125)&&($1<=18.375)) print; }'| sort -t ',' -n -k 1 > Pbins/showers1k_h_P_18-125_18-375.csv
cat showers1k_h_P.csv | awk -F"," '{if (($1>=18.375)&&($1<=18.625)) print; }'| sort -t ',' -n -k 1 > Pbins/showers1k_h_P_18-375_18-625.csv
cat showers1k_h_P.csv | awk -F"," '{if (($1>=18.625)&&($1<=18.875)) print; }'| sort -t ',' -n -k 1 > Pbins/showers1k_h_P_18-625_18-875.csv
cat showers1k_h_P.csv | awk -F"," '{if (($1>=18.875)&&($1<=19.125)) print; }'| sort -t ',' -n -k 1 > Pbins/showers1k_h_P_18-875_19.csv


cat showers1k_h_I.csv | awk -F"," '{if (($1>=16.625)&&($1<=16.875)) print; }'| sort -t ',' -n -k 1 > Ibins/showers1k_h_I_16-625_16-875.csv
cat showers1k_h_I.csv | awk -F"," '{if (($1>=16.875)&&($1<=17.125)) print; }'| sort -t ',' -n -k 1 > Ibins/showers1k_h_I_16-875_17-125.csv
cat showers1k_h_I.csv | awk -F"," '{if (($1>=17.125)&&($1<=17.375)) print; }'| sort -t ',' -n -k 1 > Ibins/showers1k_h_I_17-125_17-375.csv
cat showers1k_h_I.csv | awk -F"," '{if (($1>=17.375)&&($1<=17.625)) print; }'| sort -t ',' -n -k 1 > Ibins/showers1k_h_I_17-375_17-625.csv
cat showers1k_h_I.csv | awk -F"," '{if (($1>=17.625)&&($1<=17.875)) print; }'| sort -t ',' -n -k 1 > Ibins/showers1k_h_I_17-625_17-875.csv
cat showers1k_h_I.csv | awk -F"," '{if (($1>=17.875)&&($1<=18.125)) print; }'| sort -t ',' -n -k 1 > Ibins/showers1k_h_I_17-875_18-125.csv
cat showers1k_h_I.csv | awk -F"," '{if (($1>=18.125)&&($1<=18.375)) print; }'| sort -t ',' -n -k 1 > Ibins/showers1k_h_I_18-125_18-375.csv
cat showers1k_h_I.csv | awk -F"," '{if (($1>=18.375)&&($1<=18.625)) print; }'| sort -t ',' -n -k 1 > Ibins/showers1k_h_I_18-375_18-625.csv
cat showers1k_h_I.csv | awk -F"," '{if (($1>=18.625)&&($1<=18.875)) print; }'| sort -t ',' -n -k 1 > Ibins/showers1k_h_I_18-625_18-875.csv
cat showers1k_h_I.csv | awk -F"," '{if (($1>=18.875)&&($1<=19.125)) print; }'| sort -t ',' -n -k 1 > Ibins/showers1k_h_I_18-875_19.csv


for i in Ibins/*.csv; do  cat $i | head -n 2; echo "------"; awk 'BEGIN{print "lgE,Xmax"}1' $i > temp; cat temp > $i; echo "------"; cat $i  | head -n 2; done

for i in Pbins/*.csv; do  cat $i | head -n 2; echo "------";  awk 'BEGIN{print "lgE,Xmax"}1' $i > temp; cat temp > $i; echo "------"; cat $i  | head -n 2; done
