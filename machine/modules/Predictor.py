from tqdm import tqdm# Libreria para generar visualizacion de progreso de procesos

import logging # libreria para hacer uso de las clases de loggeo y manejo de registros de alarmas de python
import os # Libreria para utilizar estandares del sistema donde se corre el codigo
import sys # Libreria para manejar llamados al sisyema donde se corre el codigo

import json

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint

from random import seed,triangular
#import keras # Libreria para utilizar la intefaz para utilizar tensorflow
from keras.models import load_model # Libreria para importar modelo con funciones para el manejo de modelos entrenados por keras


class Predictor():
    "Clase para utilizar un modelo predictivo."

    def __init__(self, model, logger):

        self.logger = logger

        if not model.endswith('/'):
            self.folder = model+"/"
        else:
            self.folder = model
        
        plt.rcParams['agg.path.chunksize'] = 10000

    #/////////////////// Cagamos Modelo /////////////////
        model_name = os.path.basename(os.path.dirname(self.folder))
        self.model = load_model(self.folder+'models/'+model_name+'.h5') 
        
    #////////////////// Data a Graficar ////////////////
        data_record = pd.read_csv(self.folder+'train_record/record_data.csv', sep=",",dtype={"loss":np.dtype('f8') ,"val_loss":np.dtype('f8'),"t_epoch":np.dtype('f8') }, header=0)
        
        
        
        current_description="\n<<<<<<<<<<<<< Recorded Training Performance >>>>>>>>>> \n%s\n\n%s\n <><><><><><><><><><><><><><><><><><>\n" % (data_record.head(), data_record.describe())
        print(current_description)
        self.logger.info(current_description)
        data_record = data_record.loc[data_record.loss <= data_record.loss.min()*2]
        data_columns = data_record.columns
        self.loss = data_record[data_columns[data_columns=='loss']]
        self.val_loss = data_record[data_columns[data_columns=='val_loss']]
        self.t_epoch = data_record[data_columns[data_columns=='t_epoch']]

        #///////////////////////////////////////////////////////////77
        data_record = pd.read_csv(self.folder+'train_record/record_train_data.csv', sep=",",dtype=np.dtype('f8'), header=0)
        self.data_columns = data_record.columns
        self.x_train_norm = data_record[self.data_columns[0]]
        self.x_train = data_record[self.data_columns[1]]
        self.y_train = data_record[self.data_columns[2:]]

        current_description="\n<<<<<<<<<<<<< Recorded Data  >>>>>>>>>> \n%s\n\n%s\n <><><><><><><><><><><><><><><><><><>\n" % (data_record.head(), data_record.describe())
        print(current_description)
        self.logger.info(current_description)

        #/////////////////////////////////////////////////////////////
    #////////////////////////////////////////////////////
        

    def predict(self, ran):

    #///////////////// Generador del rango de Prediccion //////////
        """ minimo=float(ran[0])
        maximo=float(ran[1]) """
        #minimo=self.x_train.min()
        #maximo=self.x_train.max()
        #points=float(ran[2])
        #steps= (maximo-minimo)/points
        #self.xfit= np.arange(minimo, maximo, steps,dtype='f8')
        self.xfit = self.x_train
        
        
        

    #//////////////// Normalizacion de los puntos //////////////
        if self.folder.find("Norm") is not -1:
            self.ran = self.x_train.max() - self.x_train.min()
            self.Xfit = (self.xfit - self.x_train.min())/self.ran
            #self.Xfit = (self.xfit - self.x_train['x_train'].mean())/self.x_train['x_train'].std()
            #self.Xfit = (self.xfit - self.x_train.min())/(self.x_train.max()-self.x_train.min())
            ##self.xfit = np.linspace(-2.1,2.1) #-2, 2
            #self.Xfit = self.x_train_norm
            #self.Xfit = self.Xfit[:,np.newaxis]
            #self.x_train = self.x_train_norm
            
            #self.Xfit = self.x_train.min() + (ran*self.x_train_norm)
        else:
            self.Xfit = self.xfit[:,np.newaxis]
            
    #/////////////// Predicciones Gneradas ///////////////////
        
        self.target_p = pd.DataFrame(self.model.predict(self.Xfit), dtype='f8' )#predictor 
        self.target_p.rename(columns={c:self.data_columns[c+2] for c in self.target_p.columns.values}, inplace=True)
        current_description="\n<<<<<<<<<<<<< Prediction Data  >>>>>>>>>> \n%s\n\n%s\n <><><><><><><><><><><><><><><><><><>\n" % (self.target_p.head(10), self.target_p.describe())
        print(current_description)
        self.logger.info(current_description)
        
        headers = [col for col in self.target_p.columns.values]
        headers.insert(0,'Xfit')
        headers = ','.join(map(str,headers))
        outputdata = np.column_stack((self.Xfit,self.target_p))
        np.savetxt(self.folder+"/train_record/record_data_prediction.csv", outputdata, delimiter=",",header=headers , comments='')

        seed(7)
        p=[]
        if len(self.target_p.columns)>1:
            for col in self.target_p.itertuples():
                p.append(triangular(col.Xdown,col.Xup,col.Xmax))

            self.disp = pd.DataFrame(p,dtype='f8')
            #self.xfit = pd.DataFrame(self.xfit,dtype='f8')
            disperdata = np.column_stack((self.xfit,self.disp))
            #print(disperdata)
            np.savetxt(self.folder+"/train_record/record_data_dispercion.csv",disperdata, delimiter=",",header="lgE,Xmax", comments='')
            print( self.disp.head())
        self.graphAll()

    def graphAll(self):
        
        plt.xlabel("Epoch")
        plt.ylabel("Error")
        plt.title("Error VS Error de Validación")
        plt.grid(color='c', linestyle='-', linewidth=0.5)
        plt.plot(self.loss, label="loss")
        plt.plot(self.val_loss, label="val_loss")
        plt.legend()
        plt.savefig(self.folder+"plots/loss_evolution.png", dpi=300)
        plt.close()

        plt.ylabel("Xmax")
        plt.xlabel("lgE")
        if 'disp'in locals():
            plt.grid(color='c', linestyle='-', linewidth=0.5)
            plt.scatter(self.xfit,self.disp,c='blue',s=0.1,marker=",",alpha=0.7,linewidths=0,label='Dispersion')

            ajustment = self.LinearAdjustment(pd.Series(self.xfit,dtype='f8'),self.target_p.Xmax)

            plt.plot(self.xfit,ajustment,color='r', label='Ajuste')
            plt.legend()
            plt.savefig(self.folder+"plots/prediction-lineal.png", dpi=300)
            plt.cla()

            plt.xlabel("lgE")
            plt.ylabel("Xmax")
            plt.title("Dispersion de la predicción")
            plt.grid(color='c', linestyle='-', linewidth=0.5)
            plt.scatter(self.xfit,self.disp,c='blue',s=0.1,marker=",",alpha=0.7,linewidths=0,label='Dispersion')
            plt.legend()
            plt.savefig(self.folder+"plots/prediction-scatter.png", dpi=300)

            """ mask=self.disp <= self.target_p['Xmax']
            y=self.disp.loc[mask[0]]
            x=self.xfit.loc[mask[0]]
            print(y.shape)
            print(x.shape)
            plt.scatter(x,y,c='green',s=0.1,marker=",",alpha=0.6,linewidths=0)
            mask=self.disp >= self.target_p[[0]]
            y=self.disp.loc[mask[0]]
            x=self.xfit.loc[mask[0]]
            print(y.shape)
            print(x.shape)
            plt.scatter(x,y,c='blue',s=0.1,marker=",",alpha=0.6,linewidths=0)
            #plt.plot(self.Xfit, self.target_p , color='red', markersize=0, linewidth=1)
            plt.savefig(self.folder+"plots/prediction-scatter-div.png", dpi=300)
            plt.cla() """
            plt.title("Limites de la dispersión & Dispersión")

            colors= ['r','b','g']
            if len(self.target_p.columns)>1:
                for column in self.target_p.columns:
                    plt.plot(self.xfit, self.target_p[[column]] , colors[self.target_p.columns.get_loc(column)],markersize=0, linewidth=0.5,label='Disper. '+str(column))
            else:
                plt.plot(self.xfit, self.target_p , 'r',markersize=0, linewidth=0.5,label='Predic.') 

            plt.legend()
            plt.savefig(self.folder+"plots/prediction-scatter-range.png", dpi=300)
            plt.cla()

        plt.grid(color='c', linestyle='-', linewidth=0.5)
        plt.xlabel("lgE")
        plt.ylabel("Xmax")
        plt.title("Predicciones y Datos de entrenamiento")
        
        plt.scatter(self.x_train,self.y_train,c='blue',s=0.1,marker=",",alpha=0.3,linewidths=0,label='Training data')
        plt.legend()

        colors= ['r','b','g']
        if len(self.target_p.columns)>1:
            for column in self.target_p.columns:
                plt.plot(self.xfit, self.target_p[[column]] , colors[self.target_p.columns.get_loc(column)],markersize=0, linewidth=0.5,label='Predic. '+str(column))
        else:
            plt.plot(self.xfit, self.target_p , 'r',markersize=0, linewidth=0.5,label='Predic.') 
        
        plt.legend()
        plt.savefig(self.folder+"plots/prediction-orig-scatter.png", dpi=300)
        plt.cla()

        
        plt.grid(color='c', linestyle='-', linewidth=0.5)
        plt.xlabel("lgE")
        plt.ylabel("Xmax")
        plt.title("Predicciones")

        colors= ['r','b','g']
        if len(self.target_p.columns)>1:
            for column in self.target_p.columns:
                plt.plot(self.xfit, self.target_p[[column]] , colors[self.target_p.columns.get_loc(column)],markersize=0, linewidth=0.5,label='Predic. '+str(column))
        else:
            plt.plot(self.xfit, self.target_p , 'r',markersize=0, linewidth=0.5,label='Predic.') 
        
        plt.legend()
        plt.savefig(self.folder+"plots/prediction-range.png", dpi=300)
        
        

        
        
        plt.close()


        plt.grid(color='c', linestyle='-', linewidth=0.5)
        plt.xlabel("Epoch")
        plt.ylabel("Time per Epoch")
        plt.title("Duración del entrenamiento por Epoch")
        plt.plot(self.t_epoch, label="Time")
        plt.legend()
        plt.savefig(self.folder+"plots/epoch_time.png", dpi=300)


        """  k = int(np.ceil(1+np.log2(data.shape[0])))
        plt.xlabel("Xmax")
        plt.ylabel("Frecuencia")
        plt.title("Histograma de frecuencia de desarrollos de Xmax")
        plt.hist(data["xmax"], bins = k) #bins = [0,30,60,...,200] """



    def LinearAdjustment(self,x,y):
        print(x.describe())
        print(y.describe())


        n=x.count()
        sumx = sum(x)
        sumy = sum(y)
        sumx2 = sum(np.float_power(x,2))
        sumy2 = sum(np.float_power(y,2))
        sumxy = sum(x*y)
        meanx = x.mean()
        meany = y.mean()

        print("n: ",n)
        print("sumx: ",sumx)
        print("sumy: ",sumy)
        print("sumx2: ",sumx2)
        print("sumy2: ",sumy2)
        print("sumxy: ",sumxy)
        print("meanx: ",meanx)
        print("meany: ",meany)
        print("\n")
        
        """ 
        n:  100000
        sumx:  1787498.8453767037
        sumy:  52184620.03027344
        sumx2:  32003604.317145593 
        sumy2:  27269781878.09031
        sumxy:  934068783.4585336
        meanx:  17.87498845376704
        meany:  521.8462003027344
        m: -0.00012431589124264645 b: 521.8484224478549
        """

        m=(sumx*sumy - n*sumxy)/(np.float_power(sumx,2) - n*sumx2)
        b=meany - m*meanx
        print("m:",m,"b:",b)

        sigmax = np.sqrt(sumx2/n - np.float_power(meanx,2))
        sigmay = np.sqrt(sumy2/n - np.float_power(meany,2))
        sigmaxy = np.sqrt(sumxy/n - meanx*meany)
        print("sigmax:",sigmax)
        print("sigmay:",sigmay)
        print("sigmaxy:",sigmaxy)

        print("stdx:",x.std())
        print("stdy:",y.std())
        print("std x*y:",x.std()*y.std())
        print("R:",np.float_power((sigmaxy/(sigmax*sigmay)),2))
        print("\n")

        plt.title("Ajuste lineal\nR:"+str('%.3f'%(np.float_power((sigmaxy/(sigmax*sigmay)),2)))+" m:"+str('%.3f'%(m))+" b:"+str('%.3f'%(b)))

        df = pd.Series((m*x+b),dtype='f8')

        return df


