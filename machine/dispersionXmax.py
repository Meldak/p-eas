import getopt # Libreria para gestionar el paso de mensajes 
import sys # Libreria para manejar llamados al sisyema donde se corre el codigo
import os
from tqdm import tqdm

from numpy import genfromtxt
import numpy as np
import matplotlib.mlab as mlab


import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns


def main(argv):
#------------- variables 
    options = {}
#-------------
#------------- Extraccion de opciones 
    try:
        opts,arg = getopt.getopt(argv,"epd:",["extract,plot,data="])
    except getopt.GetoptError as exc:
        print(exc.msg)
        sys.exit(2)

    for opt, arg in opts:
        options[opt]=arg        
#------------- 
    print(options)

    if options.__contains__('-d') :
        dataset = options.get("-d")
    elif options.__contains__('--data'):
        dataset = options.get("--data")

    #files = os.listdir(proj)
    #files = sorted(files)
    #f, axes = plt.subplots(3, 3, figsize=(10, 7), sharex=True)
    if options.__contains__('-e') or options.__contains__('--extract')  :
        disperData(dataset)
    elif  options.__contains__('-p') or options.__contains__('--plot') :
        sns.set(style="darkgrid")
        """f, axes = plt.subplots(1, 1, figsize=(10, 7))
        y=0
        x=0
        
        for name in files[:9]:
            print(name)
            if(x==3):
                y+=1
                x=0
            print(x,y)
            plotmethis(name,y,x,f,axes)
            x+=1 
        #plotmethis(dataset)
        f.savefig("dispersion.png", dpi=400)"""
 



def plotmethis(data_dir,x,y,f,axes):

    #dataTr = pd.read_csv(data_dir+'train_record/record_train_data.csv', sep=",", header = 0,dtype={"x_train_norm":np.dtype('f8') ,"x_train":np.dtype('f8'),"y_train":np.dtype('f8') }) #x_train_norm,x_train,y_train
    #dataPr = pd.read_csv(data_dir+'train_record/record_data_prediction.csv', sep=",", header = 0,dtype={"Xfit":np.dtype('f8') ,"Prediction":np.dtype('f8'),"y_train":np.dtype('f8') })
    
    df = pd.read_csv(data_dir, sep=",", header = 0,dtype={"lgE":np.dtype('f8') ,"Xmax":np.dtype('f8') })
    dfs = df.sort_values(by=['lgE']).reset_index(drop=True)
    #print(df.head())
    #print(df.describe())
    #print(dfs.describe())
    print(dfs.head())

    """ df_disp = disperData(dfs)
    print(df_disp.head(10))
    print(df_disp.tail(10))
    print("Salvando...")
    name=data_dir.split("/")
    df_disp.to_csv(name[0]+"/dipersion-"+name[1], sep=',', index=False)
    print("Salvado en: "+name[0]+"/dipersion-"+name[1])
    sns.jointplot("Xup", "Xdown", data=df_disp, kind="reg", color="m", height=7) """

    #sns.jointplot("Xup", "Xdown", data=df_disp, kind="reg", color="m", height=7)
    #sns.lmplot('lgE', 'Xmax', data=df)
    #sns.distplot(xmax, hist=True, kde=False, bins=num_bins, ax=axes[x][y], color = 'cyan', hist_kws={'edgecolor':'black','alpha':0.2,'label':prim})
    
    # Tweak spacing to prevent clipping of ylabel
    #plt.legend()
    #plt.subplots_adjust(left=0.15)
    #plt.setp(axes)
    #plt.tight_layout()

    #name=prim+"-("+ str(min)+"-"+ str(max)+")"
 
def disperData(data_dir):
    df = pd.read_csv(data_dir, sep=",", header = 0,dtype={"lgE":np.dtype('f8') ,"Xmax":np.dtype('f8') })
    dfs = df.sort_values(by=['lgE']).reset_index(drop=True)
    print(dfs.head())
    print(dfs.shape)
    if 'primary' in dfs.columns:
        df = dfs.drop(columns=['primary'])
    else:
        df = dfs
    dfsize=dfs.groupby(df.lgE.tolist(),as_index=False).size().shape[0]
    print(df.shape)
    print(dfsize)
    dff = pd.DataFrame(0,index=np.arange(dfsize),columns=['lgE','Xup','Xdown'],dtype='f8')
    idx = 0
    #primer elemento
    dff.loc[idx]['lgE'] = df['lgE'][0]
    dff.loc[idx]['Xup'] = df['Xmax'][0]
    dff.loc[idx]['Xdown'] = df['Xmax'][0]
    #idx+=1 
    print("Inicia>>>>")
    for oidx,e,x in tqdm(df.itertuples(),ncols=120,total=df.shape[0]):
        if e!=dff.loc[idx]['lgE']:
            #print(dff.loc[idx])
            idx+=1
            dff.loc[idx]['lgE'] = e
            dff.loc[idx]['Xup'] = x
            dff.loc[idx]['Xdown'] = x
        elif x > dff.loc[idx]['Xup']:
            dff.loc[idx]['Xup'] = x
        elif x < dff.loc[idx]['Xdown']:
            dff.loc[idx]['Xdown'] = x
        #print(e,x)
    print("<<<<<<Termina")
    print(dff.head(10))
    print(dff.tail(10))
    print("Salvando...")
    name=data_dir.split("/")
    dff.to_csv(name[0]+"/dispersion-"+name[1], sep=',', index=False)
    print("Salvado en: "+name[0]+"/dispersion-"+name[1])

if __name__ == '__main__':
    #print (sys.argv[1:])
    main(sys.argv[1:])
