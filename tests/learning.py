import pandas as pd
import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense
from keras.models import load_model
from tqdm import tqdm



#//////////////////////////////////////////////
df = pd.read_csv('dataset/showers1k_I.csv', sep=",",dtype={"Xmax":np.dtype('f8') ,"lgE":np.dtype('f8') },header=0)
print("\n<<<<<<<<<<<<< DATAFRAME >>>>>>>>>>\n")
print(df.columns.values)
print(df.head())
print("\n<<<<<<<<<<<<< Data Description >>>>>>>>>>")
print(df.describe())
print("\n<<<<<<<<<<<<< Data Null >>>>>>>>>>")
print(df.isnull().sum())
print("\n<><><><><><><><><><><><><><><><><><>\n")

#//////////////// Separamos Datos ////////////
data_columns= df.columns
predictors = df[data_columns[data_columns=='lgE']]
target = df['Xmax']
print("-------- Predictors -------")
print(predictors.head())
print("-------- Target -------")
print(target.head())

#/////////////// Normalizamos los datos //////
print("\n<<<<<<<<<<<<< Data Normalize >>>>>>>>>>")
predictors_norm = (predictors - predictors.mean())/predictors.std() ## Puntuación estandar
#predictors_norm = 
print(predictors_norm.head())
n_cols = predictors_norm.shape[1]
print(n_cols)
print("\n<><><><><><><><><><><><><><><><><><>\n")

# Regression Model 
def regression_model():
    model = Sequential()
    model.add(Dense(1000, activation ='relu', input_shape=(n_cols,)))
    model.add(Dense(1))

    model.compile(optimizer='adam', loss='mean_squared_error')
    return model

# build the model
model = regression_model()

# fit the model
model.fit(predictors_norm, target, validation_split=0.20,batch_size= 32, epochs=100, verbose=2 )
print("\n<><><><><><><><><><><><><><><><><><>\n")
print(model.summary())
print("\n<><><><><><><><><><><><><><><><><><>\n")
model.save('models/100epo_1000N-1N-Relu.h5')
print(">Modelo Salvado.")