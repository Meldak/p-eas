# Import the necessary packages and modules
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint

df = pd.read_csv('dataset/showers1k_I.csv', sep=",",dtype={"Xmax":np.dtype('f8') ,"lgE":np.dtype('f8') },header=0)
data_columns= df.columns
predictors = df[data_columns[data_columns=='lgE']]
lgEmin=predictors['lgE'].min()
lgEmax=predictors['lgE'].max()
nlgE = len(predictors)
lgEstep = (lgEmax-lgEmin)/nlgE
print(lgEmin)
print(lgEmax)
print(nlgE)
print(lgEstep)
lgErange= np.arange(lgEmin,lgEmax,lgEstep,dtype='f8') #0.000001045137
#print(len(lgE))
target = df['Xmax']
print("-------- Predictors -------")
#print(predictors.head())
print(len(lgErange))
#print("-------- Target -------")
#print(target.head())
#print(predictors["lgE"].mean())
#print(predictors["lgE"].std())
#print(lgErange.mean())
#print(lgErange.std())
predictors_norm = (predictors - predictors["lgE"].mean())/predictors["lgE"].std()

lgEnorm = (lgErange - lgErange.mean())/lgErange.std()

#predictions=model.predict(predictors_norm)
#y=predictions[:]
#x=predictors[:]

pprint(predictors_norm)
pprint(lgEnorm)
print(len(lgEnorm))
#2392026