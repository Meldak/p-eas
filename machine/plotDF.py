import getopt # Libreria para gestionar el paso de mensajes 
import sys # Libreria para manejar llamados al sisyema donde se corre el codigo
import os

from numpy import genfromtxt
import numpy as np
import matplotlib.mlab as mlab
from pprint import pprint

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import random

from joblib import Parallel, delayed
import multiprocessing


def main(argv):
    #-----------Functions
    def save_df(df,dirname,filename,sign):
        df.to_csv(dirname+filename+sign+".csv", header=header, index=False)

    def promise(df,a,b,give):

        mask=(df['lgE']>a) & (df['lgE']<=b) & (df['Xmax']<=1000)
        showerbin=df.loc[mask]
        
        ignore= showerbin.shape[0]-give
        try:
            if(showerbin.shape[0]>ignore & ignore>0):
                randel = random.sample(range(showerbin.shape[0]),ignore)
                showerbin=showerbin.drop(showerbin.index[randel])
        except:
            print(showerbin.head())
            print(showerbin.shape)
            print(ignore)
            print(randel)
        return showerbin

    def get_homo(df,seg,give):
        minimun= df.lgE.min()
        maximun=df.lgE.max()
        dfhomo=pd.DataFrame()

        print(header)
        a=minimun
        shape=(maximun-minimun)/seg
        b=a+shape
        give=give
        segments=[]
        
        while a <= maximun:
            segments.append([a,b])
            a=b
            b=b+shape 

        dfhomo=dfhomo.append( Parallel( n_jobs=num_cores )( delayed( promise )( df,a,b,give ) for a,b in segments )) #ignore_index=True
        return dfhomo
    
    def get_bins(df,dirname,filename,primary,typ):
        
        a=16.625
        b=a+0.25
        name=0
        while a < 19.125:            
            mask=(df['lgE']>=a) & (df['lgE']<b)
            data=df.loc[mask]

            save_df(data,dirname,filename,str(str(name)+"_"+typ+"_"+primary))
            
            plt.xlabel("x")
            plt.ylabel("y")
            plt.title("bin"+str(name)+"-"+primary)
            plt.plot(data.loc[:,"lgE"],data.loc[:,"Xmax"],'b,')
            plt.legend()
            plt.savefig("./bin"+str(name)+"-"+typ+"-"+primary+".png", dpi=300)
            plt.cla()
            
            name+=1
            a=b
            b+=0.25
    
    def segment(df,per):
        if type(per)!=float:
            per=per/100

        split = int(np.trunc(df.shape[0]*per))
        return split

    #------------- variables 
    options = {}
    num_cores = multiprocessing.cpu_count()
    filename="showers_xdata_bin"
    dirname="./"
    primary="P"
#-------------
#------------- Extraccion de opciones 
    try:
        opts,arg = getopt.getopt(argv,"d:",["data="])
    except getopt.GetoptError as exc:
        print(exc.msg)
        sys.exit(2)

    for opt, arg in opts:
        options[opt]=arg        
#------------- 
#------------- 
    print(options)

    if options.__contains__('-d') :
        df_dir = options.get("-d")
    elif options.__contains__('--data'):
        df_dir = options.get("--data")

    df = pd.read_csv(df_dir, sep=",", header = 0)
    header = df.columns
    df = pd.read_csv(df_dir, sep=",", header = 0, dtype=np.dtype('f8'), usecols=lambda header : header not in ["primary"])
      
    if 'primary' in header : header=header.drop("primary")
   
    if 'primary' in df.columns:
        df = df.drop(columns=['primary'])
        print("Se elimino la columna primary")
    else:
        df = df
    #dfsize=df.groupby(df.lgE.tolist(),as_index=False).size().shape[0]
    print(df.head())
    print(df.shape)
    print(df.describe())
    print(df.isnull().sum())
    df=df.dropna()
    print(df.describe())
    print(df.isnull().sum())
    
    print(df.shape)

    df = df.sort_values(by='lgE').reset_index(drop=True)
    print(df.head(20))
    print(df.tail(20))

    #Dataset Homogeneizado --------------------------------
    dff=pd.DataFrame()
    dff=get_homo(df,30000,30)
    # dff=dff.reset_index(drop=True)
    print(dff.head(20))
    print(dff.tail(20))
    print(dff.shape)


    #Extraccion de data set de pruebas-----------------------------
    dftest=pd.DataFrame()
    dftest=get_homo(dff,10000,1)
    print(dftest.head(20))
    print(dftest.tail(20))
    print(dftest.shape)

    intersected_df = set(dff.index).intersection(dftest.index)
    print(len(intersected_df))
    dff = dff.drop(intersected_df, axis=0)
    print(dff.shape)

    

    dff=dff.reset_index(drop=True)
    dftest=dftest.reset_index(drop=True)
    #print("All data:",dff.shape[0],"-100%")
    p3= segment(dff,30)
    #print("30%:",p3)
    p5= segment(dff,50)
    #print("50%:",p5)
    p7= segment(dff,70)
    #print("70%:",p7)
   

        
    get_bins(dff,dirname,filename,primary,"training")
    get_bins(dftest,dirname,filename,primary,"test")
    



    plt.xlabel("x")
    plt.ylabel("y")
    plt.title("30%")
    plt.plot(dff.loc[:p3,"lgE"],dff.loc[:p3,"Xmax"],'r,')
    plt.legend()
    plt.savefig('./30-'+primary+'.png', dpi=300)
    plt.cla()

    plt.xlabel("x")
    plt.ylabel("y")
    plt.title("50%")
    plt.plot(dff.loc[:p5,"lgE"],dff.loc[:p5,"Xmax"],'b,')
    plt.legend()
    plt.savefig('./50-'+primary+'.png', dpi=300)
    plt.cla()

    plt.xlabel("x")
    plt.ylabel("y")
    plt.title("70%")
    plt.plot(dff.loc[:p7,"lgE"],dff.loc[:p7,"Xmax"],'g,')
    plt.legend()
    plt.savefig('./70-'+primary+'.png', dpi=300)
    plt.cla()


    plt.xlabel("x")
    plt.ylabel("y")
    plt.title("70% - 30%")
    plt.plot(dff.loc[:p7,"lgE"],dff.loc[:p7,"Xmax"],'b,')
    plt.plot(dff.loc[p7:,"lgE"],dff.loc[p7:,"Xmax"],'g,')
    plt.legend()
    plt.savefig('./70-30-'+primary+'.png', dpi=300)
    plt.cla()

    plt.xlabel("x")
    plt.ylabel("y")
    plt.title("density")
    sns.kdeplot(dff.loc[:,"lgE"], shade=True,color="b")
    plt.legend()
    plt.savefig('./density-'+primary+'.png', dpi=300)
    plt.cla()

    plt.xlabel("x")
    plt.ylabel("y")
    plt.title("Dataset Test 100%")
    plt.plot(dftest.loc[:,"lgE"],dftest.loc[:,"Xmax"],'r,')
    plt.legend()
    plt.savefig('./DT100-'+primary+'.png', dpi=300)
    plt.cla()

    #print(dff.loc[0:2][2:])
    plt.xlabel("x")
    plt.ylabel("y")
    plt.title("test")
    for row in np.arange(100):
        plt.plot(dff.loc[row][2:])
    plt.savefig('./dEdX-'+primary+'.png', dpi=300)
    plt.close()

    

    save_df(dff,dirname,filename,str("showers_xdata_ALL_H_"+primary))
    save_df(dff.loc[:p7],dirname,filename,str("showers_xdata_70per_"+primary))
    save_df(dff.loc[p7:],dirname,filename,str("showers_xdata_30per_"+primary))
    save_df(dftest,dirname,filename,str("showers_xdata_ALL_test_"+primary))





if __name__ == '__main__':
    #print (sys.argv[1:])
    main(sys.argv[1:])