from modules.EASDS import DB ## Se importa el gestor de la DB
from pprint import pprint ## para realizar una "pretty print"
import csv
from tqdm import tqdm
import numpy as np
import pandas as pd
import os
import json
import matplotlib.pyplot as plt

#tqdm.pandas(desc="Preparando datos...", ncols=120)
filename1 = "showers_Xdata_I.csv"
filename2 = "showers_Xdata_P.csv"
datasetI = open('./'+filename1,'w')
datasetP = open('./'+filename2,'w')
 
dataI=[["Xmax","lgE","dEdX","primary"]]
dataP=[["Xmax","lgE","dEdX","primary"]]

db=DB("showers1k","evarela","Ca0p1AH0")
showers = db.showers.find({},{"_id":0,"lgE":1, "Xmax":1, "Nmax":1, "zenith":1, "Xfirst":1, "Hfirts":1, "XfirstIn":1, "X0":1, "Xmx":1, "XmxdEdX":1, "dEdXmx":1,"dEdX":1, "primary":1})
#showers.json()

#pprint(list(showers))
showers =  pd.DataFrame(list(showers))
print(showers.head())
print(type(showers))
#print(type(showers.primary[1]))
#print(type(showers.dEdX[0][1]))
#print(showers.dEdX[0][1])
print(showers.shape)
#print(shower) # Imprimimos todo el shower
#showers['dEdX'] = showers['dEdX'].astype('f8')
#print(type(showers.dEdX[0][0]))
#print(type(showers[['dEdX']]))
#df.loc[df['shield'] > 6]
dataI= showers.loc[showers['primary']=="i"]
#print(type(dataI))
dataP= showers.loc[showers['primary']=="p"]
#print(dataI.describe())
#print(dataP)

#print(type(data))
with datasetI:
    print("Guardando Showers I")
    dataI.to_csv('./'+filename1,sep=',', index=False)

with datasetP:
    print("Guardando Showers P")
    dataP.to_csv('./'+filename2,sep=',', index=False)

print("Finalizado")
 # ///////////////// rutina de lectura ////////////////// 
""" dataset='./showers_dEdX_I.csv'

df = pd.read_csv(dataset, sep=",",dtype={"Xmax":np.dtype('f8') ,"lgE":np.dtype('f8')},header=0)
#print(df.dEdX[0][1:-1])
data_columns = df.columns.values
print(df.columns.values)
print(type(df.columns.values))

headers= np.append(data_columns,['Xmax','lgE'])
print(headers)
print(type(headers))
print(df.Xmax)
print(type(df.Xmax))

print(df[['Xmax']].values.tolist())
#print(type(df["Xmax"].values))
a=df[['Xmax']].values.tolist()
b=df[['lgE']].values.tolist()
#print(a)
#print(b)
c= np.append(a,b,axis=1)
c=pd.DataFrame(c, dtype='f8', columns=["Xmax","lgE"])
print(c)
plt.xlabel("lgE")
plt.ylabel("Xmax")
plt.title("Predicción")
#plt.scatter(self.x_train,self.y_train,s=8)
#plt.plot(self.Xfit, self.target_p , color='red', markersize=0, linewidth=1)
plt.plot(c.lgE,c.Xmax, 'r.')
#plt.plot(self.target_p[1], 'b.', label="lgE")
plt.legend()
plt.grid(color='c', linestyle='-', linewidth=1)
plt.savefig("./test.png")
plt.close() """
#c= np.append(c,a,axis=1)

#c=pd.DataFrame(c, dtype='f8', columns=["Xmax","lgE","other"])

#print(c) 

#print(df.describe())
""" aux2=[]
steps=range[0,96,3]
for  index, serie in enumerate(df.dEdX):
    #print(index,"::", serie[1:2])
    splited = serie[1:-1].split(',')
    aux2.append(pd.Series([float(num[d]) for d in steps],copy=True))
    
df = df.assign(dEdX = aux2)
 """
#df.describe()

