from tqdm import tqdm# Libreria para generar visualizacion de progreso de procesos
from pprint import pprint
import random


import time
import logging # libreria para hacer uso de las clases de loggeo y manejo de registros de alarmas de python
import os # Libreria para utilizar estandares del sistema donde se corre el codigo
import sys # Libreria para manejar llamados al sisyema donde se corre el codigo
import multiprocessing #Para obtener informacion de CPU
from io import StringIO
import json

import pandas as pd # Libreria para manejo de datos 
import numpy as np # Libreria para manejo y procesamiento de datos

from sklearn.model_selection import train_test_split
from keras.callbacks import EarlyStopping, History,Callback, ModelCheckpoint

from modules.MLM import MachineLearningModels as mlm

from keras.models import Model, load_model, save_model, Sequential
from keras.layers import Dense, Activation, Dropout, Input, Masking, TimeDistributed, LSTM, Conv1D
from keras.layers import GRU, Bidirectional, BatchNormalization, Reshape
from keras.optimizers import Adam
from keras.backend.tensorflow_backend import set_session
import tensorflow as tf
from tensorflow.python.client import device_lib

class Trainer():
    "Entrenador para la red neuronal con keras y CPU"    
    logger= logging
    df = pd.core.frame.DataFrame
    fit_history = History
    n_cols=0
    data_columns= 0
    predictors = pd.core.frame.DataFrame
    x_train_norm = pd.core.frame.DataFrame
    x_valid_norm = pd.core.frame.DataFrame
    target = pd.core.frame.DataFrame
    verbose=0
    x_train=pd.core.frame.DataFrame 
    x_valid=pd.core.frame.DataFrame 
    y_train=pd.core.frame.DataFrame
    y_valid=pd.core.frame.DataFrame

    
    def get_available_gpus(self):
        local_device_protos = device_lib.list_local_devices()
        return [x.name for x in local_device_protos if x.device_type == 'GPU']

    def __init__(self, logger, verbose, module_name, config=None ):
        self.logger = logger
        self.logger.info("Iniciando Entrenamiento con keras.")
        self.verbose = verbose
        self.filename = module_name
        if config!=None:
            self.modelconfig=config['model']
            self.fitconfig=config['fit']
            self.dataconfig=config['dataset']
            self.modelconfig['n_output']=len(self.dataconfig['target'])
            self.num_cores = multiprocessing.cpu_count() #Cuantos cores tiene la maquina
            os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID" 
            os.environ["CUDA_VISIBLE_DEVICES"] = self.fitconfig['GPU'] #que GPU usar


        self.GPU_devices=self.get_available_gpus()
        config = tf.ConfigProto(intra_op_parallelism_threads=self.num_cores,
                        inter_op_parallelism_threads=self.num_cores, 
                        allow_soft_placement=True,
                        #log_device_placement = True,
                        device_count = {'CPU' : self.num_cores,
                                        'GPU' :len(self.GPU_devices)}
                    )
        config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
        sess = tf.Session(config=config)
        set_session(sess)
    
        
    def setData(self, dataset):
        "Admite el path el archivo de datos en formato csv."

        self.logger.info("Extrayendo dataset de %s" % dataset)
        #tqdm.pandas(desc="Leyendo dataset", ncols=120)
        #self.df = pd.read_csv(dataset, sep=",",dtype={"Xmax":np.dtype('f8') ,"lgE":np.dtype('f8') },header=0)       
        self.df = pd.read_csv(dataset, sep=",", dtype=np.dtype('f8'),usecols=self.dataconfig["target"]+self.dataconfig["predictor"], header=0)
        print(self.df.shape)
        
        mask = (self.df[self.dataconfig["datacut"]] > self.dataconfig['dataMin']) & (self.df[self.dataconfig["datacut"]] <= self.dataconfig['dataMax'])
        self.df = self.df.loc[mask]

        print(self.df.shape)

        df_values = self.df.columns.values
        df_head = self.df.head()
        df_describe = self.df.describe()
        df_isnull = self.df.isnull().sum()
        
        current_description = "\n<<<<<<<<<<<<< DATAFRAME >>>>>>>>>>\n%s \n\n%s \n<<<<<<<<<<<<< Data Description >>>>>>>>>>\n %s \n<<<<<<<<<<<<< Data Null >>>>>>>>>>\n%s \n<><><><><><><><><><><><><><><><><><>\n" % (df_values, df_head, df_describe, df_isnull)
    
        self.logger.info(current_description) 
        if self.verbose == 1:
            print(current_description)

        

        self.dataPrepare()
    
    def dataPrepare(self):
    #//////////////// Separamos Datos ////////////
        #tqdm.pandas(desc="Preparando predictores [lgE]", ncols=120)

        self.data_columns= self.df.columns
        #self.predictors = self.df[self.data_columns[self.data_columns== ','.join(map(str,{col for col in self.dataconfig['predictor']}))]]
        self.predictors = self.df.loc[ : ,self.dataconfig['predictor']]
        #tqdm.pandas(desc="Preparando targets [Xmax]", ncols=120)
        #self.target = self.df[self.data_columns[self.data_columns=='Xmax']]

        self.target = self.df[self.dataconfig['target']]

        predictors_head = self.predictors.head()
        target_head = self.target.head()

        current_description = "\n-------- Colums ----------\n%s\n-------- Predictors -------\n%s\n-------- Target -------\n%s\n" % (self.data_columns,predictors_head, target_head)
       
        self.logger.info(current_description)
        if self.verbose == 1:
            print(current_description)

        self.x_train, self.x_valid, self.y_train, self.y_valid = train_test_split(self.predictors, self.target, test_size=self.fitconfig["test_size"], shuffle= True, random_state= 7)

    #/////////////// Normalizamos los datos //////
        #tqdm.pandas(desc="Normalizando predictores", ncols=120)
        
        if self.modelconfig["normalize"]:
            if self.modelconfig["fun_activ"]== "relu":
                d1=0
                d2=1
            elif self.modelconfig["fun_activ"]== "tanh":
                d1=-5
                d2=5
            else:
                d1=0
                d2=1
            ran_train = self.x_train.max() - self.x_train.min()
            ran_valid = self.x_valid.max() - self.x_valid.min()
                
            self.x_train_norm = (((self.x_train - self.x_train.min())*(d2-d1))/ran_train)+d1
            self.x_valid_norm = (((self.x_valid - self.x_valid.min())*(d2-d1))/ran_valid)+d1
            #self.x_train_norm = (self.x_train - self.x_train.mean())/self.x_train.std()
            #self.x_valid_norm = (self.x_valid - self.x_valid.mean())/self.x_valid.std()
            #self.x_train_norm = (self.x_train / self.x_train.mean())
            #self.x_valid_norm = (self.x_valid / self.x_valid.mean())
            #self.x_train_norm = (self.x_train - self.x_train.min())/ran_train
            #self.x_valid_norm = (self.x_valid - self.x_valid.min())/ran_valid
            #self.x_train_norm = ((self.x_train - self.x_train.min())*(d2-d1)/ran_train)+d1
            
        else:
            self.x_train_norm = self.x_train 
            self.x_valid_norm = self.x_valid
        
        x_train_norm_head  =  self.x_train_norm.head()
        x_train_norm_shape =  self.x_train_norm.shape
        x_valid_norm_head  =  self.x_valid_norm.head()
        x_valid_norm_shape =  self.x_valid_norm.shape

        self.n_cols =  self.x_train_norm.shape[1]


        current_description = "\n<<<<<<<<<<<<< Data Normalize >>>>>>>>>> \n#Training Data\n%s\n\n%s\n\n #Validation Data\n%s\n\n%s\n<><><><><><><><><><><><><><><><><><>\n" % (x_train_norm_head, x_train_norm_shape,x_valid_norm_head, x_valid_norm_shape)
        self.logger.info(current_description)
        if self.verbose == 1:
            print(current_description)
        
            
    def trainModel(self,folder):
        # build the model
        
        
        machine = mlm(self.logger)
            
        model = machine.linear_regression(self.n_cols,self.modelconfig)

        early_stopping = EarlyStopping(monitor='val_loss', mode = 'min', patience=10)

        time_callback = TimeHistory()
        #self.fitconfig["batch_size"]=int(np.trunc(self.x_train_norm.shape[0]/self.fitconfig["epochs"]))
        current_description = "\n<<<<<<<<<<<<< Batch Size >>>>>>>>>>\n %s\n\n <<<<<<<<<<<<< Epoch >>>>>>>>>>\n %s\n\n<><><><><><><><><><><><><><><><><><>\n" % (self.fitconfig["batch_size"],self.fitconfig["epochs"])
        self.logger.info(current_description)
        if self.verbose == 1:
            print(current_description)


        if self.fitconfig["early_stopping"]:
            self.fit_history = model.fit( self.x_train_norm, self.y_train, validation_data = (self.x_valid_norm, self.y_valid), batch_size =self.fitconfig["batch_size"], epochs=self.fitconfig["epochs"], verbose=self.fitconfig["verbose"], callbacks=[time_callback,early_stopping])
        else:
            self.fit_history = model.fit( self.x_train_norm, self.y_train, validation_data = (self.x_valid_norm, self.y_valid), batch_size =self.fitconfig["batch_size"], epochs=self.fitconfig["epochs"], verbose=self.fitconfig["verbose"], callbacks=[time_callback])

            
        epoch_time=time_callback.times

        #headers=[self.dataconfig['predictor']+"-norm",self.dataconfig['predictor'],self.dataconfig['target']]#,x_valid,y_valid"
        #headers=",".join(map(str,({t+"-norm" for t in self.dataconfig['predictor']},{ p for p in self.dataconfig['predictor']},{ p for p in self.dataconfig['target']}))).replace(" ","").replace("{","").replace("}","").replace("'","")
        if self.modelconfig["normalize"]:
            outputdata = np.column_stack((self.x_train_norm,self.x_train))
            headers=[ x+"-norm" for x in self.data_columns if x in self.dataconfig['predictor'] ]
            headers=np.append(headers, [ x for x in self.data_columns if x in self.dataconfig['predictor'] ])
        else:
            headers=[ x for x in self.data_columns if x in self.dataconfig['predictor'] ]
            outputdata = self.x_train
        headers=np.append(headers, [p for p in self.dataconfig['target']])
        #print(headers)
        outputdata = np.column_stack((outputdata,self.y_train))
        #np.savetxt(folder+"/train_record/record_train_data.csv", outputdata, delimiter=",",header=headers, comments='')
        pd.DataFrame(outputdata,dtype=np.dtype('f8')).to_csv(folder+"/train_record/record_train_data.csv",sep=',', header=headers, index=False)

        headers=["loss","val_loss","t_epoch"]
        outputdata = np.column_stack((self.fit_history.history["loss"],self.fit_history.history["val_loss"]))
        outputdata = np.column_stack((outputdata,epoch_time))
        #np.savetxt(folder+"/train_record/record_data.csv", outputdata, delimiter=",",header=headers, comments='')
        pd.DataFrame(outputdata,dtype=np.dtype('f8')).to_csv(folder+"/train_record/record_data.csv",sep=',', header=headers, index=False)
        
        #///////////
        

        origStdout = sys.stdout
        outputBuf = StringIO()
        sys.stdout = outputBuf

        model.summary()

        sys.stdout = origStdout
        modelDescription = outputBuf.getvalue()
        self.logger.info(modelDescription)

        if self.verbose == 1:
            print("\n<><><><><><><><><><><><><><><><><><>\n")
            model.summary()
            print("\n<><><><><><><><><><><><><><><><><><>\n")
        model.save(folder+'/models/'+self.filename+'.h5')
        self.logger.warning(">>> Modelo Salvado <<<.\n")
        self.logger.warning(">>> models/"+self.filename+".h5 <<<.\n")
        self.logger.warning("\n<><><><><><><><><><><><><><><><><><>")
        self.logger.warning("\n<><><><><><><><><><><><><><><><><><>\n\n\n")
        if self.verbose == 1:
            print(">>> Modelo Salvado <<<.\n\n\n")
            self.logger.warning(">>> models/"+self.filename+".h5 <<<.\n")
            print("\n<><><><><><><><><><><><><><><><><><>")
            print("\n<><><><><><><><><><><><><><><><><><>\n\n\n")
        

    

class TimeHistory(Callback):
    def on_train_begin(self, logs={}):
        self.times = []

    def on_epoch_begin(self, batch, logs={}):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, batch, logs={}):
        self.times.append(time.time() - self.epoch_time_start)
