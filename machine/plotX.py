import getopt # Libreria para gestionar el paso de mensajes 
import sys # Libreria para manejar llamados al sisyema donde se corre el codigo
import os

from numpy import genfromtxt
import numpy as np
import matplotlib.mlab as mlab
from pprint import pprint

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

distances={}
distances["Iron"]={}
distances["Proton"]={}
def main(argv):
    #------------- variables 
    options = {}
#-------------
#------------- Extraccion de opciones 
    try:
        opts,arg = getopt.getopt(argv,"d:",["data="])
    except getopt.GetoptError as exc:
        print(exc.msg)
        sys.exit(2)

    for opt, arg in opts:
        options[opt]=arg        
#------------- 
    print(options)

    if options.__contains__('-d') :
        bin_dir = options.get("-d")
    elif options.__contains__('--data'):
        bin_dir = options.get("--data")

    files = os.listdir(bin_dir)
    files = sorted(files)
    #f, axes = plt.subplots(3, 3, figsize=(10, 7), sharex=True)
    f, axes = plt.subplots(3, 3, figsize=(10, 7))
    y=0
    x=0
    num=0
    for name in files[:9]:
        print(name)
        if(x==3):
            y+=1
            x=0
        print(x,y)
        plotmethis(name,y,x,f,axes,num)
        x+=1
        num+=1
    
    f.savefig("I-9bins-red-line.png", dpi=400)
    
    #f, axes = plt.subplots(3, 3, figsize=(10, 7), sharex=True)
    f, axes = plt.subplots(3, 3, figsize=(10, 7))
    y=0
    x=0
    num=0
    for name in files[10:19]:
        print(name)
        if(x==3):
            y+=1
            x=0
        print(x,y)
        plotmethis(name,y,x,f,axes,num)
        x+=1
        num+=1

    f.savefig("P-9bins-red-line.png", dpi=400)

    pprint(distances)

def plotmethis(data_dir,x,y,f,axes,num):
    
    if not data_dir.endswith('/'):
        data_dir = data_dir+"/"
    else:
        data_dir = data_dir

    #dirs = data_dir.split("/")

    if not data_dir.startswith('I'):
        prim = "Proton"
    else:
        prim = "Iron"

    
    dataTr = pd.read_csv("results/bins2/"+data_dir+'train_record/record_train_data.csv', sep=",", header = 0,dtype={"lgE-norm":np.dtype('f8') ,"lgE":np.dtype('f8'),"Xmax":np.dtype('f8') }) #x_train_norm,x_train,y_train
    dataPr = pd.read_csv("results/bins2/"+data_dir+'train_record/record_data_prediction.csv', sep=",", header = 0,dtype={"Xfit":np.dtype('f8') ,"Xmax":np.dtype('f8')})
    dataTr.sort_values(by=['lgE'])
    dataPr.sort_values(by=['Xfit'])
    print(dataTr.describe())
    print(dataPr.describe())
    xmax = dataTr["Xmax"] - dataPr["Xmax"]
    
    if prim == "Proton":
        mask = (xmax < 250 )

    else:
        mask = (xmax < 100 )
        
    xmax = xmax.loc[mask]
    print(xmax.describe())
    
    # example data
    
    num_bins = 50
    max = np.around(dataTr["lgE"].max(), decimals=3)
    min = np.around(dataTr["lgE"].min(), decimals=3)

    
    sns.despine(left=True)
    

    axes[x][y].set_xlabel(r'$\Delta X_{max} [g/cm^2]$')
    axes[x][y].set_ylabel('N')
    axes[x][y].set_title("lgE - ("+ str(min) +" - "+ str(max) +")")
    axes[x][y].grid(color='c', linestyle='-', linewidth=1)

    hist=sns.distplot(xmax, hist=True, kde=False, bins=num_bins, ax=axes[x][y], color = 'r', hist_kws={'edgecolor':'black','alpha':0.2,'label':prim})
    plt.cla()
    ax = sns.kdeplot(xmax, shade=True,ax=axes[x][y], color="b")
    
    axx , axy = ax.lines[0].get_data()

    idx=np.where(axy==axy.max())[0][0]
    my=axy.item(idx)
    mx=axx.item(idx)
    print(idx)
    print(my)
    print(mx)

    high=0.0
    for h in hist.patches:
        if float(h.get_height())>= high:
            high=h.get_height()
    
    print(">>>>>>>>>",high,"<<<<<<<<<") 
    distances[prim]["bin"+str(num)]={}
    distances[prim]["bin"+str(num)]["Y"]= high
    distances[prim]["bin"+str(num)]["X"]= abs(mx)
    # Tweak spacing to prevent clipping of ylabel
   
    plt.legend()
    plt.subplots_adjust(left=0.15)
    plt.setp(axes)
    plt.tight_layout()

    #name=prim+"-("+ str(min)+"-"+ str(max)+")"
 

if __name__ == '__main__':
    #print (sys.argv[1:])
    main(sys.argv[1:])
