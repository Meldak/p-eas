import pandas as pd
import numpy as np
import keras

import matplotlib.pyplot as plt

model = keras.models.load_model("models/100epo_1000N-1N-Relu.h5")
#df = pd.read_csv('dataset/showers1k_I.csv', sep=",",dtype={"Xmax":np.dtype('f8') ,"lgE":np.dtype('f8') },header=0)
#data_columns= df.columns
#predictors = df[data_columns[data_columns=='lgE']]
#lgEmin=predictors['lgE'].min()
lgEmin = 15
#lgEmax=predictors['lgE'].max()
lgEmax = 20
nlgE = 10000#len(predictors)
lgEstep = (lgEmax-lgEmin)/nlgE
#target = df['Xmax']
lgErange= np.arange(lgEmin,lgEmax,lgEstep,dtype='f8') 
#lgErange= np.arange(predictors['lgE'].min(),predictors['lgE'].max(),0.000001045137,dtype='f8')

#print("-------- Predictors -------")
#print(predictors.head())
#print("-------- Target -------")
#print(target.head())

#predictors_norm = (predictors - predictors.mean())/predictors.std()
lgEnorm = (lgErange - lgErange.mean())/lgErange.std()

predictions=model.predict(lgEnorm)
#predictions2=model.predict(predictors_norm)
#------ save predictions -----
outputdata=np.column_stack((predictions,lgErange))
np.savetxt("predictions/prediction_Over600ERROR2.csv", outputdata, delimiter=",")
#-----------------------------
y=predictions[:]
x=lgErange[:]

#y2=predictions2[:]
#x2=predictors[:]

#plt.scatter(predictors,target,s=8)#0.3

plt.plot(x,y,
        color='red',
        markersize=0,
        linewidth=1,#1
)

#plt.plot(x2,y2,
#        color='green',
#        markersize=0,
#        linewidth=1,#1
#)

plt.xlim(lgEmin, lgEmax)

plt.xlabel("lgE")
plt.ylabel("Xmax")

plt.title("Predictor Xmax - I")

plt.show()
#plt.savefig('./tes_model-50epo-1HLy-764N.jpg')