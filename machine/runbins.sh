#!/bin/bash

i=0
in="Ibin"
pn="Pbin"
for confI in configs/bins/I/*.json;
do
#more $confI | tail -n 6 ;
python Machine-Xmax.py --train --model-config $confI --name-sign $in$i
i=$((i+1))
done

i=0
for confP in configs/bins/P/*.json;
do
#more $confP | tail -n 6 ;
python Machine-Xmax.py --train --model-config $confP --name-sign $pn$i
i=$((i+1))
done