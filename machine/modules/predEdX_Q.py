from pprint import pprint ## para realizar una "pretty print"
import csv
from tqdm import tqdm
import os

import pandas as pd # Libreria para manejo de datos 
import numpy as np # Libreria para manejo y procesamiento de datos

dataset='../../datasets/showers_dEdX_I.csv'
dataset='../../datasets/Q_showers_dEdX_I.csv'
df = pd.read_csv(dataset, sep="|",dtype={"Xmax":np.dtype('f8') ,"lgE":np.dtype('f8')},header=0)
df['dEdX'] = df['dEdX'].str.split(',')
aux = []
for counter, lst in enumerate(df['dEdX']):
    aux.append(pd.Series(lst, dtype='f8'))
df = df.assign(dEdX = pd.Series(aux))

print(df.describe)
print(df["dEdX"].describe)
print(df.shape) 
