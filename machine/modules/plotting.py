from tqdm import tqdm# Libreria para generar visualizacion de progreso de procesos

import logging # libreria para hacer uso de las clases de loggeo y manejo de registros de alarmas de python
import os # Libreria para utilizar estandares del sistema donde se corre el codigo
import sys # Libreria para manejar llamados al sisyema donde se corre el codigo

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint

class Plotter():
    "Clase Plotter para sacar todos los grafos representativos de los datos"
    

    dataset = pd.core.base
    res_data = pd.core.base

    def origin_data(self, dataset):
        self.dataset = dataset

    def result_data(self, data):
        self.res_data = data

    def scatter_plotting(self,data):

        data.plot(kind="scatter", x="lgE", y="Xmax")
        plt.savefig("plots/scatter.png")
      

    def frecuency_plotter(self, data):
        k = int(np.ceil(1+np.log2(data.shape[0])))
        plt.xlabel("Xmax")
        plt.ylabel("Frecuencia")
        plt.title("Histograma de frecuencia de desarrollos de Xmax")
        plt.hist(data["xmax"], bins = k) #bins = [0,30,60,...,200]

        plt.savefig("plots/frecuency.png")

    def get_pots(self, data):
        figure, axs = plt.subplots(2,2, sharey=True, sharex=True)
        data.plot(kind="scatter", x="lgE", y="Xmax", ax=axs[0][0])
