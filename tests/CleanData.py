import pandas as pd
import numpy as np
from tqdm import tqdm

#//////////////////////////////////////////////
df = pd.read_csv('dataset/showers1k_I.csv', sep=",",dtype={"Xmax":np.dtype('f8') ,"lgE":np.dtype('f8') },header=0)
print("\n<<<<<<<<<<<<< DATAFRAME >>>>>>>>>>\n")
print(df.columns.values)
print(df.head())
print("\n<<<<<<<<<<<<< Data Description >>>>>>>>>>")
print(df.describe())
print("\n<<<<<<<<<<<<< Data Null >>>>>>>>>>")
print(df.isnull().sum())
print("\n<><><><><><><><><><><><><><><><><><>\n")

#//////////////// Separamos Datos ////////////
data_columns= df.columns
predictors = df[data_columns[data_columns=='Xmax']]
target = df['lgE']
print("-------- Predictors -------")
print(predictors.head())
print("-------- Target -------")
print(target.head())

#/////////////// Normalizamos los datos //////
#predictors_norm = (predictors - predictors.mean())/predictors.std()

#n_cols = predictors_norm.shape()
