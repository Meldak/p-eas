#!/bin/bash

ib0="Ibin0-relu-4hl1out7000epo-I-Norm"
ib1="Ibin1-relu-4hl1out7000epo-I-Norm"
ib2="Ibin2-relu-4hl1out7000epo-I-Norm"
ib3="Ibin3-relu-4hl1out7000epo-I-Norm"
ib4="Ibin4-relu-4hl1out7000epo-I-Norm"
ib5="Ibin5-relu-4hl1out7000epo-I-Norm"
ib6="Ibin6-relu-4hl1out7000epo-I-Norm"
ib7="Ibin7-relu-4hl1out7000epo-I-Norm"
ib8="Ibin8-relu-4hl1out7000epo-I-Norm"
ib9="Ibin9-relu-4hl1out7000epo-I-Norm"

pb0="Pbin0-relu-4hl1out7000epo-P-Norm"
pb1="Pbin1-relu-4hl1out7000epo-P-Norm"
pb2="Pbin2-relu-4hl1out7000epo-P-Norm"
pb3="Pbin3-relu-4hl1out7000epo-P-Norm"
pb4="Pbin4-relu-4hl1out7000epo-P-Norm"
pb5="Pbin5-relu-4hl1out7000epo-P-Norm"
pb6="Pbin6-relu-4hl1out7000epo-P-Norm"
pb7="Pbin7-relu-4hl1out7000epo-P-Norm"
pb8="Pbin8-relu-4hl1out7000epo-P-Norm"
pb9="Pbin9-relu-4hl1out7000epo-P-Norm"

python Machine-Xmax.py -p 16.600,16.900,10000 -m results/binsH/$ib0 
python Machine-Xmax.py -p 16.800,17.160,10000 -m results/binsH/$ib1 
python Machine-Xmax.py -p 17.050,17.420,10000 -m results/binsH/$ib2 
python Machine-Xmax.py -p 17.300,17.700,10000 -m results/binsH/$ib3 
python Machine-Xmax.py -p 17.570,17.910,10000 -m results/binsH/$ib4 
python Machine-Xmax.py -p 17.800,18.180,10000 -m results/binsH/$ib5 
python Machine-Xmax.py -p 18.070,18.410,10000 -m results/binsH/$ib6 
python Machine-Xmax.py -p 18.300,18.680,10000 -m results/binsH/$ib7 
python Machine-Xmax.py -p 18.575,18.900,10000 -m results/binsH/$ib8 
python Machine-Xmax.py -p 18.875,19.150,10000 -m results/binsH/$ib9 

python Machine-Xmax.py -p 16.600,16.900,10000 -m results/binsH/$pb0 
python Machine-Xmax.py -p 16.800,17.160,10000 -m results/binsH/$pb1 
python Machine-Xmax.py -p 17.050,17.420,10000 -m results/binsH/$pb2 
python Machine-Xmax.py -p 17.300,17.700,10000 -m results/binsH/$pb3 
python Machine-Xmax.py -p 17.570,17.910,10000 -m results/binsH/$pb4 
python Machine-Xmax.py -p 17.800,18.180,10000 -m results/binsH/$pb5 
python Machine-Xmax.py -p 18.070,18.410,10000 -m results/binsH/$pb6 
python Machine-Xmax.py -p 18.300,18.680,10000 -m results/binsH/$pb7 
python Machine-Xmax.py -p 18.575,18.900,10000 -m results/binsH/$pb8 
python Machine-Xmax.py -p 18.875,19.150,10000 -m results/binsH/$pb9 
