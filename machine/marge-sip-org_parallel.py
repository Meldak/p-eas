import getopt # Libreria para gestionar el paso de mensajes 
import sys # Libreria para manejar llamados al sisyema donde se corre el codigo
import os
from tqdm import tqdm

from numpy import genfromtxt
import numpy as np
import matplotlib.mlab as mlab


import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from joblib import Parallel, delayed
import multiprocessing


def main(argv):
    #------------ Functions 
    def promise(dfs2,lge,xup,xdown,idx):
        data = dfs2.loc[dfs2['lgE'].isin([lge])].copy()
        data['Xup']=xup
        data['Xdown']=xdown
        if idx == 0:
            return pd.DataFrame(data)
        else:
            return data
    #------------- variables 
    options = {}
    #-------------
    #------------- Extraccion de opciones 
    try:
        opts,arg = getopt.getopt(argv,"a:b:",["dataA=,dataB="])
    except getopt.GetoptError as exc:
        print(exc.msg)
        sys.exit(2) 

    for opt, arg in opts:
        options[opt]=arg        
    #------------- 
    print(options)

    if options.__contains__('-a') :
        ds1 = options.get("-a")
    elif options.__contains__('--dataA'):
        ds1 = options.get("--dataA")

    if options.__contains__('-b') :
        ds2 = options.get("-b")
    elif options.__contains__('--dataB'):
        ds2 = options.get("--dataB")

    
    df1 = pd.read_csv(ds1, sep=",", header = 0,dtype=np.dtype('f8'))
    dfs1 = df1.sort_values(by=['lgE']).reset_index(drop=True)

    df2 = pd.read_csv(ds2 , sep=",", header = 0,dtype={"lgE":np.dtype('f8') ,"Xmax":np.dtype('f8') })
    if 'primary' in df2.columns:
        df2 = df2.drop(columns=['primary'])
    dfs2 = df2.sort_values(by=['lgE']).reset_index(drop=True)
    dfs2 = dfs2[dfs2.columns[[1,0]]]

    print(dfs1.describe())
    print(dfs2.describe())
    print(df2.shape)
    print(df1.shape)
    num_cores = multiprocessing.cpu_count()
    dfA_size =dfs1.shape[0]
    df2 = df2.loc[(df2['Xmax'] <= 900)]
    df1 = df1.loc[(df1['Xup'] <= 900)]
    print(df2.shape)
    print(df1.shape)
    for idx,lge,xup,xdown in tqdm(dfs1.itertuples(),ncols=120,total=dfA_size):
    #for idx,lge,xup,xdown in dfs1.itertuples():
        #print("\033c", end="")
        #print(idx," ",i)
        if idx==0:
            dff = promise(dfs2,lge,xup,xdown,idx)
        else:
            break
        
    dff=dff.append(Parallel(n_jobs=num_cores)(delayed(promise)(dfs2,lge,xup,xdown,idx) for idx,lge,xup,xdown in tqdm(dfs1[1:].itertuples(),ncols=120,total=dfA_size)),ignore_index=True)
    #dff=dff.append(Parallel(n_jobs=num_cores)(delayed(promise)(dfs2,lge,xup,xdown,idx) for idx,lge,xup,xdown in dfs1[1:].itertuples()),ignore_index=True)
    print(dff.describe())
    print(dff.shape)

    print("<< Termino >>")
    print("Salvando...")
    name=ds2.split("/")
    dff.to_csv(name[0]+"/alldata-dipersion-parallel-"+name[1], sep=',', index=False)
    print("Salvado en: "+name[0]+"/alldata-dipersion-parallel-"+name[1])
            
    
      




if __name__ == '__main__':
    #print (sys.argv[1:])
    main(sys.argv[1:])
