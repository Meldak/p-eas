import getopt # Libreria para gestionar el paso de mensajes 
import sys # Libreria para manejar llamados al sisyema donde se corre el codigo
import os
from tqdm import tqdm

from numpy import genfromtxt
import numpy as np
import matplotlib.mlab as mlab


import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns


def main(argv):
#------------- variables 
    options = {}
#-------------
#------------- Extraccion de opciones 
    try:
        opts,arg = getopt.getopt(argv,"a:b:",["dataA=,dataB="])
    except getopt.GetoptError as exc:
        print(exc.msg)
        sys.exit(2)

    for opt, arg in opts:
        options[opt]=arg        
#------------- 
    print(options)

    if options.__contains__('-a') :
        ds1 = options.get("-a")
    elif options.__contains__('--dataA'):
        ds1 = options.get("--dataA")

    if options.__contains__('-b') :
        ds2 = options.get("-b")
    elif options.__contains__('--dataB'):
        ds2 = options.get("--dataB")

    
    df1 = pd.read_csv(ds1, sep=",", header = 0,dtype=np.dtype('f8'))
    dfs1 = df1.sort_values(by=['lgE']).reset_index(drop=True)

    df2 = pd.read_csv(ds2 , sep=",", header = 0,dtype={"lgE":np.dtype('f8') ,"Xmax":np.dtype('f8') })
    if 'primary' in df2.columns:
        df2 = df2.drop(columns=['primary'])
    dfs2 = df2.sort_values(by=['lgE']).reset_index(drop=True)
    dfs2 = dfs2[dfs2.columns[[1,0]]]

    print(dfs1.head())
    print(dfs2.head())
    i=0
    #for idx,lge,xup,xdown in tqdm(dfs1.itertuples(),ncols=120,total=dfs1.shape[0]):
    for idx,lge,xup,xdown in dfs1.itertuples():
        #print("\033c", end="")
        #print(idx," ",i)
        if idx==0:
            data = dfs2.loc[dfs2['lgE'].isin([lge])].copy()
            data['Xup']=xup
            data['Xdown']=xdown
            dff= pd.DataFrame(data)
            #print(dff)
            #print("-----\n")
        else:
            data = dfs2.loc[dfs2['lgE'].isin([lge])].copy()
            data['Xup']=xup
            data['Xdown']=xdown
            dff=dff.append(data,ignore_index=True)
            #print(dff.head(50))
            #print("-----\n")
        i+=1 
        
        
    print(dff.shape)
    print("<< Termino >>")
    print("Salvando...")
    name=ds2.split("/")
    dff.to_csv(name[0]+"/alldata-dipersion-"+name[1], sep=',', index=False)
    print("Salvado en: "+name[0]+"/alldata-dipersion-"+name[1])
            
        
        
      




if __name__ == '__main__':
    #print (sys.argv[1:])
    main(sys.argv[1:])
