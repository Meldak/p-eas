import getopt # Libreria para gestionar el paso de mensajes 
import sys # Libreria para manejar llamados al sisyema donde se corre el codigo
import os

from numpy import genfromtxt
import numpy as np
import matplotlib.mlab as mlab
from pprint import pprint

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import random

from joblib import Parallel, delayed
import multiprocessing

from keras.models import load_model

def main(argv):
    #-----------Functions
    def promise(df,a,b,give):

        mask=(df['lgE']>a) & (df['lgE']<=b)
        showerbin=df.loc[mask]
        
        ignore= showerbin.shape[0]-give
        try:
            if(showerbin.shape[0]>ignore & ignore>0):
                randel = random.sample(range(showerbin.shape[0]),ignore)
                showerbin=showerbin.drop(showerbin.index[randel])
        except:
            print(showerbin.head())
            print(showerbin.shape)
            print(shape)
            print(ignore)
            print(randel)
        return showerbin 

    #------------- variables 
    options = {}
    num_cores = multiprocessing.cpu_count()
#-------------
#------------- Extraccion de opciones 
    try:
        opts,arg = getopt.getopt(argv,"m:d:",["model=","dataframe="])
    except getopt.GetoptError as exc:
        print(exc.msg)
        sys.exit(2)

    for opt, arg in opts:
        options[opt]=arg        
#------------- 
#------------- 
    print(options)

    if options.__contains__('-m') :
        modelf = options.get("-m")
    elif options.__contains__('--model'):
        modelf = options.get("--model")

    if options.__contains__('-d') :
        df_dir = options.get("-d")
    elif options.__contains__('--dataframe'):
        df_dir = options.get("--dataframe")

    if not modelf.endswith('/'):
        folder = modelf+"/"
    else:
        folder = modelf

    model_name = os.path.basename(os.path.dirname(folder))
    model = load_model(folder+'models/'+model_name+'.h5') 

    #df_RT = pd.read_csv(folder+'train_record/record_data.csv', sep=",",dtype={"loss":np.dtype('f8') ,"val_loss":np.dtype('f8'),"t_epoch":np.dtype('f8') }, header=0)
    df = pd.read_csv(folder+'train_record/record_train_data.csv', sep=",",dtype=np.dtype('f8'), header=0, nrows=1)
    header = df.columns
    print(header)
    train = pd.read_csv(folder+'train_record/record_train_data.csv', sep=",",dtype=np.dtype('f8'), usecols=[ x for x in header if "norm" in x ] )
    df = pd.read_csv(df_dir, sep=",", header = 0, dtype=np.dtype('f8'), usecols=lambda header : header not in ["primary","Xmax"])
    
    header = df.columns
    
    print(header)
    print(df.head())
    print(df.shape)
    print(df.describe())
    
    minimun= df.lgE.min()
    maximun=df.lgE.max()
    dff=pd.DataFrame()

    a=minimun
    shape=(maximun-minimun)/100 #40000
    b=a+shape
    give=10 #25
    segments=[]
    
    while a <= maximun:
        segments.append([a,b])
        a=b
        b=b+shape 
    
    dff=dff.append( Parallel( n_jobs=num_cores )( delayed( promise )( df,a,b,give ) for a,b in segments ), ignore_index=True )#ignore_index=True
    

    d1=0
    d2=1
    ran = train.max() - train.min()
    train_norm = (((train - train.min())*(d2-d1))/ran_train)+d1
    #Xfit = (xfit - train.min())/ran
    
    print("\n<><><><><><><><><><><><><><><><>\n")
    print(dff.head())
    print(dff.shape)
    print(dff.describe())



    """ prediction = pd.DataFrame(model.predict(dff), dtype='f8' )

    print("\n<><><><><><><><><><><><><><><><>\n")
    print(prediction.head())
    print(prediction.shape)
    print(prediction.describe()) """











if __name__ == '__main__':
    #print (sys.argv[1:])
    main(sys.argv[1:])