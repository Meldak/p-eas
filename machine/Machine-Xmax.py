
import getopt # Libreria para gestionar el paso de mensajes 
import logging # libreria para hacer uso de las clases de loggeo y manejo de registros de alarmas de python
from pprint import pprint # Libreria para imprimir estructuras de datos especiales (ej: Json)
from pathlib import Path # Libreria para
import os # Libreria para utilizar estandares del sistema donde se corre el codigo
import sys # Libreria para manejar llamados al sisyema donde se corre el codigo

import json

#from modules.EASDS import Connection # Libreria (propia) para establecer coneccion con la base de datos de datasets de Lluvias atmosfericas del LNS
from modules.Trainer import Trainer
from modules.Predictor import Predictor

def main(argv):
#------------- variables 
    options = {}
    modelConfig = None
#-------------
#------------- Extraccion de opciones 
    try:
        opts,arg = getopt.getopt(argv,"htp:m:v:c:n:",["help","train","predict=","model-folder=","verbose=","model-config=","name-sign="])
    except getopt.GetoptError as exc:
        print(exc.msg)
        sys.exit(2)

    for opt, arg in opts:
        options[opt]=arg        
#------------- 
    print(options)

#------------- Validaciones

    verbose = 0

    if options.__contains__('-v') :
        verbose = int(options.get("-v"))
    elif options.__contains__('--verbose'):
        verbose = int(options.get("--verbose"))

    if options.__contains__('-c') :
        Jspath = options.get("-c")
    elif options.__contains__('--model-config'):
        Jspath = options.get("--model-config")
    else:
        Jspath=None

    if Jspath!=None:
        with open(Jspath) as json_file:  
            modelConfig = json.load(json_file)

#------------- Configuracion folders
        if options.__contains__('--name-sign') or options.__contains__('-n'):
            name_sign = options.get('--name-sign')
            if name_sign == None:
                name_sign = options.get('-n')
            name_sign = name_sign+"-"
        else:
            name_sign=""
        modelConfig['model']['n_output']=len(modelConfig['dataset']['target'])
        if modelConfig['model']['normalize']:
            train_name = name_sign+modelConfig['model']['fun_activ']+'-'+str(len(modelConfig['model']['hide_ly']))+'hl'+str(modelConfig['model']['n_output'])+'out'+str(modelConfig['fit']['epochs'])+'epo-'+modelConfig["dataset"]["primary"]+'-Norm'
        else:
            train_name = name_sign+modelConfig['model']['fun_activ']+'-'+str(len(modelConfig['model']['hide_ly']))+'hl'+str(modelConfig['model']['n_output'])+'out'+str(modelConfig['fit']['epochs'])+'epo-'+modelConfig["dataset"]["primary"]
        train_folder = "./results/"+train_name
    
    else:
    
        if options.__contains__('--model-folder'): 
            model_folder = options.get('--model-folder')
        elif options.__contains__('-m'):
            model_folder = options.get('-m')
        else:
            msn="AVISO: Proporcione la ruta a la carpeta del proyecto del modelo."
            print(msn)
            sys.exit(2)

        train_folder = model_folder


    if not os.path.isdir('./results'):
        os.mkdir('./results')

    if not os.path.isdir(train_folder):
        os.mkdir(train_folder)

    if not os.path.isdir(train_folder+'/models'):
        os.mkdir(train_folder+'/models')

    if not os.path.isdir(train_folder+'/plots'):
        os.mkdir(train_folder+'/plots')

    if not os.path.isdir(train_folder+'/train_record'):
        os.mkdir(train_folder+'/train_record')

    if not os.path.isdir(train_folder+'/logs'):
        os.mkdir(train_folder+'/logs')
        
    logger = setup_logger('started', train_folder+'/logs/started.log')
   
   

#------------- Configuracion Logging
    logger.warning("\n\n--------------------------------------------")
    logger.info("Los argumentos son:\n%a" % argv)

#-------------

#------------- Ejeccuicon 
#////////////////////////// HELP ////////////////////////////////////
    if  options.__contains__('-h') or options.__contains__('--help'):
        print('EJ: machine.py -[option] <value>')
        print('The options could be:')
        print('--train | -t : for do a training.')
        print('--predict [init,end,points] | -p [init,end,points] : for define the prediccion and the range of data to predict.')
        sys.exit()

#////////////////////////// TRAIN ////////////////////////////////////
    elif options.__contains__('-t') or options.__contains__('--train'):
        logger.info("Iniciando entrenamiento")
        trainlog = setup_logger('Traning', train_folder+'/logs/Traning.log')
        if(modelConfig!=None):
            trainer = Trainer(trainlog, verbose,train_name, modelConfig)
        else:
            sys.exit(2)
       
        trainer.setData(modelConfig["dataset"]["path"])
        trainer.trainModel(train_folder) 

            
#////////////////////////// PREDICT ////////////////////////////////////          
    elif options.__contains__('-p') or options.__contains__('--predict') :
        logger.info("Iniciando predicciones")

        if not model_folder.endswith('/'):
            model_folder = model_folder+"/"
        
        if options.__contains__('-p'):
            ran = options.get("-p")
        elif options.__contains__("--predict"):
            ran = options.get("--predict")
         

        if ran=='':
            msn="AVISO: Proporcione el rango de datos a predecir:\nEj: --predict [init,end,points] | -p [init,end,points]"
            logger.error(msn)
            print(msn)
            sys.exit(2)
        else:
            ran = [x.strip() for x in ran.split(',')]


        #print(type(ran))
        predictlog = setup_logger('Predictor', model_folder+'/logs/predictor.log')

        predictor=Predictor(model_folder, predictlog)
        predictor.predict(ran)

   
##////////////////////////////////////////////////////////////////////////////////
##//////////// Funcion de Configuracion logger para rutina de inicio /////////////
def setup_logger(name, log_file, level=logging.INFO):
    """Function setup as many loggers as you want"""

    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    handler = logging.FileHandler(log_file)        
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger
##///////////////////////////////////////////////////////////////////////////////


if __name__ == '__main__':
    #print (sys.argv[1:])
    main(sys.argv[1:])