# -*- coding: utf-8 -*-
"""
Created on Thu Feb  7 12:00:58 2019

@author: alumno
"""

import pandas as pd
import numpy as np
from keras.models import load_model
import matplotlib.pyplot as plt

model = load_model('tes_model-lgE_to_Xmax.h5')

df = pd.read_csv('showers1k_I.csv', sep=",",dtype={"Xmax":np.dtype('f8') ,"lgE":np.dtype('f8') },header=0)

predictors = df['lgE']
target = df['Xmax']

predictors_norm = (predictors - predictors.mean())/predictors.std()

target_p = model.predict(predictors_norm)

plt.plot(predictors, target, 'bo', predictors, target_p, 'g+')
