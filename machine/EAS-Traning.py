from tqdm import tqdm # Libreria para generar visualizacion de progreso de procesos

import getopt # Libreria para gestionar el paso de mensajes 
import logging # libreria para hacer uso de las clases de loggeo y manejo de registros de alarmas de python
from pprint import pprint # Libreria para imprimir estructuras de datos especiales (ej: Json)
from pathlib import Path # Libreria para
import os # Libreria para utilizar estandares del sistema donde se corre el codigo
import sys # Libreria para manejar llamados al sisyema donde se corre el codigo

from modules.EASDS import Connection # Libreria (propia) para establecer coneccion con la base de datos de datasets de Lluvias atmosfericas del LNS

import pandas as pd # Libreria para manejo de datos 
import numpy as np # Libreria para manejo y procesamiento de datos
import keras # Libreria para utilizar la intefaz para utilizar tensorflow
from keras.models import Sequential # importacion de modulos prefabs de keras 
from keras.layers import Dense # Libreria para importar modelos de capas pefabs de keras
from keras.models import load_model # Libreria para importar modelo con funciones para el manejo de modelos entrenados por keras

def main(argv):
    logging.debug("\n\n--------------------------------------------")
    logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    logging.debug(argv)
    try:
        opts,arg = getopt.getopt(argv,"h:d:c:",["directory=","collection="]) 
    except getopt.GetoptError:
        print('UpdateRoot.py -d <rootsdirectory> -c <DBcollection> ')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('UpdateRoot.py -d <rootsdirectory> -c <DBcollection> ')
            sys.exit()
        elif opt in ('-d', "--directory"):
            rootDir=os.path.abspath(arg)
            #dirName = os.path.basename(arg)
            dirName = rootDir.split("/")
            logging.info("Root Dir: "+dirName[-1])
            print("Root Dir: "+dirName[-1])
            if os.path.exists(arg):
                logging.info("**EXIST: "+rootDir)
                print("**EXIST: ",rootDir)
            else:
                logging.warning("X Root dir NOT found: "+rootDir)
                sys.exit()
        elif opt in ('-c', "--collection"):
            collection = str(arg)

    #scratch="/scratch2/evarela/"



if __name__ == '__main__':
    print (sys.argv[1:])
    main(sys.argv[1:])