###########################################################################
#####   __  __    __    ____   ____    ____  _  _                        ##
#####  (  \/  )  /__\  (  _ \ ( ___)  (  _ \( \/ )                       ##
#####   )    (  /(__)\  )(_) ) )__)    ) _ < \  /                        ##
#####  (_/\/\_)(__)(__)(____/ (____)  (____/ (__)                        ##
#####              ███▄ ▄███▓▓█████  ██▓    ▓█████▄  ▄▄▄       ██ ▄█▀    ##
#####              ▓██▒▀█▀ ██▒▓█   ▀ ▓██▒    ▒██▀ ██▌▒████▄     ██▄█▒    ## 
#####              ▓██    ▓██░▒███   ▒██░    ░██   █▌▒██  ▀█▄  ▓███▄░    ##
#####              ▒██    ▒██ ▒▓█  ▄ ▒██░    ░▓█▄   ▌░██▄▄▄▄██ ▓██ █▄    ##
#####              ▒██▒   ░██▒░▒████▒░██████▒░▒████▓  ▓█   ▓██▒▒██▒ █▄   ##
#####              ░ ▒░   ░  ░░░ ▒░ ░░ ▒░▓  ░ ▒▒▓  ▒  ▒▒   ▓▒█░▒ ▒▒ ▓▒   ##
#####              ░  ░      ░ ░ ░  ░░ ░ ▒  ░ ░ ▒  ▒   ▒   ▒▒ ░░ ░▒ ▒░   ##
#####              ░      ░      ░     ░ ░    ░ ░  ░   ░   ▒   ░ ░░ ░    ##
#####                     ░      ░  ░    ░  ░   ░          ░  ░░  ░      ##
#####                                         ░                          ##
###########################################################################

from pymongo import MongoClient
import logging
from pprint import pprint
#logging.debug('This message should go to the log file')
#logging.info('So should this')
#logging.warning('And this, too')
class DB():
    "Connection whit EAS data base for request of data only"

    dataset="" # Coleccion de datos a la cual se desea acceder
    user="" # Usuario registrado en el EAS_DataSet
    secret="" # Contraseña del usario
    uri = "" # Direccion de autenticacion a la DB EAS_DataSet

    def __init__(self, dataset,username, password): # Al instancias la Clase DB se establece una conexion a la DB
        self.dataset=dataset
        self.user=username
        self.secret=password
        self.uri = "mongodb://"+self.user+":"+self.secret+"@148.228.13.37:27017/EAS_DataSet" # 
        self.OpenDB()   # Nueva conexion a la DB

    
    def Search(self, primary=None, model=None, energy=None, fields=None): # Funcion de busquedas predefinidas
        
        if primary != None and model!=None and energy!=None and fields!=None: # Busqueda con 4 arcumentos

            pass
            if len(energy)<1 :
                print(">> El valor de rango de energia requiere ser una lista con la energia menor y energia mayor. Ej:[Emin,Emax]")
            else :
                acttives={"_id":0}
                for field in fields:
                    acttives[field]=1
                data = self.showers.find({"primary":primary, "model":model,"lgE":{'$gte':energy[0],'$lte':energy[1]}},acttives)

        elif primary != None and model!=None and energy!=None: # Busqueda con 3 arcumentos
            print(str(energy[0]))
            print(str(energy[1]))
            if len(energy)>1 :
                data = self.showers.find({"primary":primary, "model":model,"lgE":{'$gte':energy[0],'$lte':energy[1]}},{"_id":0})
            else:
                print("El valor de rango de energia requiere ser una lista con la energia menor y energia mayor. Ej:[Emin,Emax]")

        elif primary != None and model!=None: # Busqueda con 2 arcumentos
            data = self.showers.find({"primary":primary,"model":model},{"_id":0})
             
        elif primary != None : # Busqueda con 1 arcumentos
            data = self.showers.find({"primary":primary},{"_id":0})
           # {"primary":"p"},{"_id":1,"primary":1}
        
        return data
    
    def getAllFields(self,fields=None): # Busca solo por campos
        
        if fields!=None : # Busqueda con 1 arcumentos
            acttives={"_id":0}
            for field in fields:
                acttives[field]=1
            data = self.showers.find({},acttives)
        
        return data

    def CloseDB(self): # Rutina para cerrar la conexion a la DB
        self.client.close()
    
    def OpenDB(self): # Rutina para establecer una conexon a la DB
        self.client= MongoClient(self.uri)
        self.db=self.client["EAS_DataSet"]
        self.showers=self.db[self.dataset]


class Connection():
    "Conection whit EAS data base in Venus host"

    #logging.basicConfig(filename='./logs/EASDS.log',level=logging.DEBUG)
    exist=True
    user="" # Usuario registrado en el EAS_DataSet
    secret="" # Contraseña del usario
    uri = "mongodb://"+user+":"+secret+"@148.228.13.37:27017/"
    dataset=''
    isOpen=False

    def __init__(self,dataset,username,password):
        self.dataset=dataset
        self.user=username
        self.secret=password
        self.uri = "mongodb://"+self.user+":"+self.secret+"@148.228.13.37:27017/" 
        self.openDB()
    def insertShower(self,sh):
        self.showers.insert_one(sh)
        #self.closeDB()
    
    def closeDB(self):
        self.client.close()
        self.isOpen=False

    def openDB(self):
        self.client= MongoClient(self.uri)
        self.db=self.client["EAS_DataSet"]
        self.showers=self.db[self.dataset]
        self.isOpen=True
        self.exist=True
    
    def existDoc(self,doc):

        if(self.exist==True):
            data=self.showers.find({"sh_num":doc[0],"rootFile":doc[1],"model":doc[2],"primary":doc[3]}).count()
            if(data==1):
                self.exist=True
                #logging.debug('Este documento ya esta en la DB:\n'+str(doc))
            elif(data==0):
                self.exist=False
                logging.debug('Este es el primer documento nuevo en la DB:\n'+str(doc))
            else:
                logging.warning('Un documento con '+str(data)+' registros aparecio:\n'+str(doc))
                self.exist=True
        else:
            self.exist=self.exist       
        
        return self.exist
    
